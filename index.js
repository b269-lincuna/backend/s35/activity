//Data Persistence via Mongoose ODM

// ODM - Object Document Mapper - is a tool that translate objects in code to documents for use in document-based databases such as MongoDB

// Mongoose - an ODM library that manages data relatioship, validates chemas, and simplifies MongoDb document manipulation via the use of models.

//Models - a programming interface for querying or manipulating a database. A Mongoose model contains methods that simplify such operations

// Schemas - a representation of a document's structure. it also contains a document's expected properties and data types.

//CODE - ALONG - DISCUSSION

// s35 folder gitbash 
// >> npm init -y 
// >> npm install express mongoose 
// >> npm install -g nodemon
// 	-g means globally for auto update
// >> touch .gitignore index.js
//  - create .gitignore file and index.js file

// http is included and supported in express module 
const express = require("express");

// Mongoose is a package that allows creating of Schemas to our model/data structures
// Also has access to a number of methods for manipulating our database

const mongoose = require("mongoose");

const app = express(); //server
const port = 3001; // port

// Connecting to MongoDB Atlas [START] *****************

// connection string can get in MongoDB Atlas connect application connection string
mongoose.connect("mongodb+srv://jamepaullincuna:admin123@zuitt-bootcamp.k9jjqkm.mongodb.net/s35?retryWrites=true&w=majority",
	{
		// allows us to avoid any current and future errors while connecting to MongoDB
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Allows to handle errors when the initial connection is established
let db = mongoose.connection;

// console.error.bind(console) allows us to print errors in the browser console and terminal
// "connection error" is the message that will display if any error is encountered
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database"));

// Connecting to MongoDB Atlas [END] *****************

// Middlewares - software that provides common services and capabilities to applications outside of what's offered by the operating system

// allows your app toread JSON data
app.use(express.json());

// allows you app to read data from any other forms
app.use(express.urlencoded({extended:true}));

// [SECTION] Mongoose Schemas

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// [SECTION] Mongoose Models
// model names must be in singular form and capitalize

const Task = mongoose.model("Task",taskSchema);


//Creation of Task Application
// Create a new task

/* BUSINESS LOGIC SAMPLE
1. Add a functionality to check if there are duplicate tasks
	- If the task already exists in the database, we return an error
	- If the task doesn't exist in the database, we add it in the database

2. The task data will be coming from the request's body (postman)

3. Create a new Task object with a "name" field/property

4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req,res) => {
	// REFERENCE: https://mongoosejs.com/docs/models.html
	Task.findOne({name:req.body.name}).then((result,err) => {
		
		if(result != null && result.name==req.body.name){
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});

			newTask.save().then((savedTask, saveErr)=>{
				if (saveErr){return console.error(saveErr);}
				else{return res.status(201).send("New task created");}
			})
		}
	})
});

//Getting all the tasks
/*BUSINESS LOGIC
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

app.get("/tasks",(req,res) => {
	Task.find({}).then((result,err)=>{
		if (err){
			return console.log(err);
		} else {
			return res.status(200).json({data:result});
		}
	})
});


//***********************************************************
//******************** ACTIVITY [START] *********************
//***********************************************************

const userSchema = new mongoose.Schema({
	userName: String,
	password: String
});

const User = mongoose.model("User", userSchema);

app.post("/signup", (req,res) => {
	// REFERENCE: https://mongoosejs.com/docs/models.html
	User.findOne({userName:req.body.userName, password:req.body.password}).then((result,err) => {
		
		if(result != null && (result.userName==req.body.userName && result.password==req.body.password)){
			return res.send("User already registered");
		} else {
			let newUser = new User({
				userName: req.body.userName,
				password: req.body.password
			});

			newUser.save().then((savedUser, saveErr) => {
				saveErr ? console.error(saveErr) : res.send("New user registered!")
			})
		}
	})
});

app.get("/users",(req,res) => {
	User.find({}).then((result,err)=>{
		err ? console.log(err) : res.json({data:result})
	})
});

//***********************************************************
//******************** ACTIVITY [ END ] *********************
//***********************************************************

// code for basic server
app.listen(port,() => console.log(`Server running at port ${port}.`))


